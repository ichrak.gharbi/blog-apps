//models
const Article = require('../models/article');

module.exports = {
  findAll: async (req, res) => {
    let page = req.query.page ? req.query.page : 1;
    let perpage = 10;

    try {
      await Article.paginate({}, { page, limit: perpage }, function (err, result) {
        console.log(result);
        res.json(result);
      });
    } catch (err) {
      res.status(500).json({
        message: err.message
      });
    }
  },

  create: async (req, res) => {
    try {
      let author = req.body.author ? req.body.author : 'Ichrak';

      const article = new Article({
        title: req.body.title,
        content: req.body.content,
        author: author,
        tags: req.body.tags,
        category: req.body.category,
        isPublished: true
      });

      let a = await article.save();
      res.status(201).json(a);
    } catch (err) {
      res.status(500).json({
        message: err.message
      });
    }
  },

  update: async (req, res) => {
    try {
      let id = req.params.id;
      const article = await Article.findByIdAndUpdate(
        id,
        {
          $set: {
            title: req.body.title,
            content: req.body.content
          }
        },
        { new: true }
      );

      res.json(article);
    } catch (err) {
      return res.status(404).json({
        status: 404,
        message: 'Article not found'
      });
    }
  },

  delete: async (req, res) => {
    const id = req.params.id;

    const result = await Article.deleteOne({ _id: id });
    console.log(result);
    if (result.deletedCount) {
      return res.json({
        message: 'Article deleted successuffly'
      });
    } else {
      return res.status(404).json({
        status: 404,
        message: 'Article not found'
      });
    }
  }
};
