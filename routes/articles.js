const router = require('express').Router();
const { check } = require('express-validator');

//middlewares
const validationResult = require('../middlewares/validationResult');

//controllers

const articlesController = require('../controllers/articles.controller');

// get

router.get('/api/articles', articlesController.findAll);

// post

router.post(
  '/api/articles',
  [check('title').notEmpty(), check('content').isLength({ min: 5 }), check('isPublished'), validationResult],
  articlesController.create
);

//put
router.put('/api/articles/:id', [check('title').notEmpty(), check('content').isLength({ min: 5 }), validationResult], articlesController.update);

//delete
router.delete('/api/articles/:id', articlesController.delete);

module.exports = router;
