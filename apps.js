const express = require('express');
const mongoose = require('mongoose');

//Router
const articlesRoutes = require('./routes/articles');

const app = express();

app.use(express.json());

// connection to database
mongoose
  .connect('mongodb+srv://ichrak-GH:11353675@cluster0.wdzyu.mongodb.net/blog?retryWrites=true&w=majority', {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false
  })
  .then(() => console.log('connected'))
  .catch((err) => console.log('error', err));

//hello world
app.get('/', (req, res) => {
  res.json('hello world');
});

app.use(articlesRoutes);

app.listen(3000, () => {
  console.log('listening on port 3000');
});
