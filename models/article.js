const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');

const articleSchema = new mongoose.Schema({
  title: {
    type: String,
    required: true
  },
  content: {
    type: String,
    required: true
  },
  author: String,
  category: {
    type: String,
    enum: ['web', 'mobile', 'dev']
  },
  tags: [String],
  date: {
    type: Date,
    default: Date.now
  },
  isPublished: Boolean
});

articleSchema.plugin(mongoosePaginate);

const Article = mongoose.model('Article', articleSchema);

module.exports = Article;
